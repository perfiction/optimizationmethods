﻿using System;
using System.Collections.Generic;

namespace Core
{
    public class ArrayGraph : Graph
    {
        double[,] _graph;

        public ArrayGraph() { }

        public ArrayGraph(int vertices)
        {
            Vertices = vertices;
            _graph = new double[vertices, vertices];

            Matching = new Dictionary<int, int>();
            Groups = new Dictionary<int, int>();
            GroupCount = 0;
        }

        public ArrayGraph(int vertices, bool directed)
        {
            Vertices = vertices;
            _graph = new double[vertices, vertices];
            IsDirected = directed;
        }

        public ArrayGraph(int vertices, Dictionary<int, int> edges)
        {
            this.IsWeighted = false;
            this.IsDirected = false;

            Vertices = vertices;

            _graph = new double[vertices, vertices];

            foreach (KeyValuePair<int, int> kv in edges)
                AddEdge(kv.Key, kv.Value);

            Matching = new Dictionary<int, int>();
            Groups = new Dictionary<int, int>();
            GroupCount = 0;
        }

        public override void AddEdge(int vertex1, int vertex2, double weight = 1.0)
        {
            if (!IsWeighted && weight != 1.0)
                throw new ArgumentException("Weight is not available in non-weighted graphs.");

            _graph[vertex1, vertex2] = weight;
            if (!IsDirected)
                _graph[vertex2, vertex1] = weight;

            Edges++;
        }

        public override void RemoveEdge(int vertex1, int vertex2)
        {
            _graph[vertex1, vertex2] = 0;
            if (!IsDirected)
                _graph[vertex2, vertex1] = 0;

            Edges--;
        }

        public override List<KeyValuePair<int,int>> GetAllEdges()
        {
            List<KeyValuePair<int,int>> edges = new List<KeyValuePair<int,int>>();

            for(int i = 0; i < Vertices; i++)
                for(int j = 0; j < Vertices; j++)
                    if(_graph[i,j] != 0.0)
                        edges.Add(new KeyValuePair<int, int>(i,j));

            return edges;
        }

        public override bool HasNeighbors(int vertex)
        {
            for (int i = 0; i < Vertices; i++)
                if (_graph[vertex, i] != 0.0)
                    return true;

            return false;
        }

        public override void Print()
        {
            base.Print();

            for (int i = 0; i < Vertices; i++)
            {
                for (int j = 0; j < Vertices; j++)
                {
                    Console.Write(_graph[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public override object Clone()
        {
            ArrayGraph copy = new ArrayGraph(Vertices);

            copy.SetProperties(new[] { IsDirected, IsWeighted, IsConnected, IsComplete, IsBipartite });

            for (int i = 0; i < Vertices; i++)
                for (int j = 0; j < Vertices; j++)
                    copy._graph[i, j] = _graph[i, j];

            foreach (KeyValuePair<int, int> kv in Matching)
                copy.Matching.Add(kv.Key, kv.Value);

            foreach(KeyValuePair<int,int> kv in Groups)
                copy.Groups.Add(kv.Key,kv.Value);

            copy.GroupCount = GroupCount;

            return copy;
        }

        public override int CalculateDegree(int vertex)
        {
            if (IsDirected)
                throw new InvalidOperationException("Can't calculate degree in directed graph.");

            int degree = 0;

            for (int i = 0; i < Vertices; i++)
                if (_graph[vertex, i] != 0.0)
                    degree++;


            return degree;
        }

        public override int CalculateIncomingEdges(int vertex)
        {
            int edges = 0;
            for (int i = 0; i < Vertices; i++)
                if (_graph[i, vertex] != 0.0)
                    edges++;

            return edges;
        }

        public override int CalculateOutgoingEdges(int vertex)
        {
            int edges = 0;
            for (int i = 0; i < Vertices; i++)
                if (_graph[vertex, i] != 0.0)
                    edges++;

            return edges;
        }

        public override void SetVertexCount(int vertices)
        {
            this.Vertices = vertices;
            _graph = new double[vertices, vertices];

            Matching = new Dictionary<int, int>();
            Groups = new Dictionary<int, int>();
            GroupCount = 0;
        }

        public override bool HasEdge(int vertex1, int vertex2)
        {
            return _graph[vertex1, vertex2] != 0.0;
        }

        public override double EdgeCost(int vertex1, int vertex2)
        {
            return _graph[vertex1, vertex2];
        }

        public override List<int> GetNeighbors(int vertex)
        {
            List<int> output = new List<int>();
            for (int i = 0; i < Vertices; i++)
                if (_graph[vertex, i] != 0.0)
                    output.Add(i);

            return output;
        }
    }
}
