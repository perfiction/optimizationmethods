﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Core
{
    public static class FileReader
    {
        public static Graph ReadGraphFile(string filePath, Type outputGraphType)
        {
            StreamReader reader = File.OpenText(filePath);

            string s = reader.ReadLine();
            bool[] properties;
            int vertices;

            if (s.Contains("#PROPERTIES"))
                properties = s.Split(' ')[1].Split(';').Select(str => str == "1").ToArray();
            else throw new FileLoadException("Error while loading properties");

            if ((s = reader.ReadLine()).Contains("#VERTICES"))
                vertices = int.Parse(s.Split(' ')[1]);
            else throw new FileLoadException("Error while loading vertices count");

            Graph output = null;
            if (outputGraphType == typeof(ArrayGraph))
                output = new ArrayGraph(vertices);
            else if (outputGraphType == typeof(ListGraph))
                output = new ListGraph(vertices);
            else
                throw new ArgumentException("Unknown graph type - " + outputGraphType);

            output.SetProperties(properties);

            if ((s = reader.ReadLine()).Contains("#EDGES"))
            {
                while ((s = reader.ReadLine()) != null)
                {
                    string[] line = s.Split(' ');
                    output.AddEdge(int.Parse(line[0]), int.Parse(line[1]), int.Parse(line[2]));
                }
            }

            return output;
        }

        public static class FromTextReader
        {
            private static readonly string START_GRAPH = "#START_GRAPH";
            private static readonly string END_GRAPH = "#END_GRAPH";
            private static readonly string PROPERTIES = "#PROPERTIES";
            private static readonly string VERTICES = "#VERTICES";
            private static readonly string EDGES = "#EDGES";
            private static readonly string END_EDGES = "#END_EDGES";
            private static readonly string GROUPS = "#GROUPS";
            private static readonly string END_GROUPS = "#END_GROUPS";
            private static readonly string CONNECTIONS = "#CONNECTIONS";
            private static readonly string END_CONNECTIONS = "#END_CONNECTIONS";

            public static List<Graph> ReadGraphsFromFile(string path, Type graphType)
            {
                var graphs = new List<Graph>();
                var singleGraphLines = new List<string>();
                bool isGraphBeingRead = false;
                foreach (var line in File.ReadLines(path))
                {
                    if (!line.StartsWith("//") && !String.IsNullOrWhiteSpace(line))
                    {
                        if (line.StartsWith(END_GRAPH))
                        {
                            if (isGraphBeingRead)
                            {
                                var graph = ReadGraphFromLines(singleGraphLines, graphType);
                                graphs.Add(graph);
                                isGraphBeingRead = false;
                                singleGraphLines.Clear();
                            }
                        }

                        if (isGraphBeingRead)
                        {
                            singleGraphLines.Add(line);
                        }

                        if (line.StartsWith(START_GRAPH))
                        {
                            isGraphBeingRead = true;
                        }
                    }
                }
                return graphs;
            }

            public static Graph ReadGraphFromLines(List<string> lines, Type graphType)
            {
                Graph output = null;
                if (graphType == typeof(ArrayGraph))
                    output = new ArrayGraph();
                else if (graphType == typeof(ListGraph))
                    output = new ListGraph();
                else
                    throw new ArgumentException("Unknown graph type - " + graphType);

                bool areEdgesBeingRead = false;
                bool areGroupsBeingRead = false;
                bool areConnectionsBeingRead = false;
                int groupId = 0;
                foreach (var line in lines)
                {
                    if (!line.StartsWith("//"))
                    {
                        if (line.StartsWith(END_EDGES))
                        {
                            areEdgesBeingRead = false;
                        }

                        if (line.StartsWith(END_GROUPS))
                        {
                            areGroupsBeingRead = false;
                        }

                        if (line.StartsWith(END_CONNECTIONS))
                        {
                            areConnectionsBeingRead = false;
                        }

                        if (line.StartsWith(PROPERTIES))
                        {
                            GetProperties(output, line);
                        }

                        if (line.StartsWith(VERTICES))
                        {
                            var vertexCount = int.Parse(line.Split(' ').Last());
                            output.SetVertexCount(vertexCount);
                        }

                        if (areEdgesBeingRead)
                        {
                            AddEdgeFromLine(output, line);
                        }

                        if (areGroupsBeingRead)
                        {
                            AddGroupFromLine(output, line, groupId);
                            groupId++;
                        }

                        if (areConnectionsBeingRead)
                        {
                            AddConnectionFromLine(output, line);
                        }

                        if (line.StartsWith(EDGES))
                        {
                            areEdgesBeingRead = true;
                        }

                        if (line.StartsWith(GROUPS))
                        {
                            areGroupsBeingRead = true;
                        }

                        if (line.StartsWith(CONNECTIONS))
                        {
                            areConnectionsBeingRead = true;
                        }
                    }
                }
                return output;
            }

            private static void AddConnectionFromLine(Graph graph, string line)
            {
                var ids = line.Split(' ').Select(x => int.Parse(x));
                int sourceNodeId = ids.First();
                int destinationNodeId = ids.Last();
                //TODO connection - lista taka sama jak krawedzi
                graph.AddEdgeToMatching(sourceNodeId, destinationNodeId);
            }

            private static void AddGroupFromLine(Graph graph, string line, int groupId)
            {
                var nodeIds = line.Split(' ').Select(x => int.Parse(x));
                //TODO Group - wierzcholek ma pole groupId
                graph.AddVerticesToGroup(nodeIds, groupId);
            }

            private static void AddEdgeFromLine(Graph graph, string line)
            {
                string[] values = line.Split(' ').ToArray();
                int sourceNodeId = int.Parse(values[0]);
                int destinationNodeId = int.Parse(values[1]);
                double weight = double.Parse(values[2]);

                graph.AddEdge(sourceNodeId, destinationNodeId, weight);
            }

            private static void GetProperties(Graph graph, string line)
            {
                bool[] properties = line.Split(' ').Last().Split(';').Select(x =>
                {
                    return int.Parse(x) == 1;
                }).ToArray();
                if (properties.Count() != 5)
                {
                    throw new ArgumentException("Wrong number of properties (expected - 5)");
                }

                graph.SetProperties(properties);
            }
        }
    }
}
