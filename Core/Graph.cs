﻿using System;
using System.Collections.Generic;

namespace Core
{
    public abstract class Graph : ICloneable
    {
        public static Graph Create(Type type, int vertices)
        {
            if (type == typeof(ArrayGraph))
                return new ArrayGraph(vertices);
            else
                return new ListGraph(vertices);
        }

        public int Vertices { get; protected set; }
        public int Edges { get; protected set; }

        #region Properties
        public bool IsDirected { get; protected set; } //Skierowany
        public bool IsWeighted { get; protected set; } //Ważony
        public bool IsConnected { get; private set; } //Spójny
        public bool IsComplete { get; protected set; } //Pełny
        public bool IsBipartite { get; protected set; } //Dwudzielny
        #endregion

        #region Bipartite
        public Dictionary<int, int> Matching;
        public Dictionary<int, int> Groups;
        public int GroupCount { get; set; }

        public void AddEdgeToMatching(int vertex1, int vertex2)
        {
            Matching.Add(vertex1, vertex2);
            Matching.Add(vertex2, vertex1);
        }

        public bool AreMatched(int vertex1, int vertex2)
        {
            return Matching[vertex1] == vertex2 && Matching[vertex2] == vertex1;
        }

        public bool HasMatching(int vertex)
        {
            return Matching.ContainsKey(vertex);
        }

        public void AddVerticesToGroup(IEnumerable<int> vertices, int groupId)
        {
            foreach (int vertex in vertices)
                Groups.Add(vertex, groupId);
            if (groupId > GroupCount)
                GroupCount++;
        }
        #endregion

        public abstract void SetVertexCount(int vertices);

        public abstract bool HasEdge(int vertex1, int vertex2);
        public abstract double EdgeCost(int vertex1, int vertex2);
        public abstract void AddEdge(int vertex1, int vertex2, double weight = 1.0);
        public abstract void RemoveEdge(int vertex1, int vertex2);
        public abstract List<KeyValuePair<int, int>> GetAllEdges();

        public abstract int CalculateDegree(int vertex);
        public abstract int CalculateIncomingEdges(int vertex);
        public abstract int CalculateOutgoingEdges(int vertex);

        public abstract bool HasNeighbors(int vertex);
        public abstract List<int> GetNeighbors(int vertex);

        public virtual void Print()
        {
            Console.WriteLine("Graph properties:");
            Console.WriteLine("Directed: {0}", IsDirected);
            Console.WriteLine("Weighted: {0}", IsWeighted);
            Console.WriteLine("Connected: {0}", IsConnected);
            Console.WriteLine("Complete: {0}", IsComplete);
            Console.WriteLine("Bipartite: {0}", IsBipartite);
            Console.WriteLine();
        }

        public void SetProperties(bool[] properties)
        {
            IsDirected = properties[0];
            IsWeighted = properties[1];
            IsConnected = properties[2];
            IsComplete = properties[3];
            IsBipartite = properties[4];
        }

        public abstract object Clone();
    }
}
