﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Core
{
    public class ListGraph : Graph
    {
        private List<SortedDictionary<int, double>> _graph;

        public ListGraph() { }

        public ListGraph(int vertices)
        {
            Vertices = vertices;

            _graph = new List<SortedDictionary<int, double>>();
            for (int i = 0; i < Vertices; i++)
                _graph.Add(new SortedDictionary<int, double>());

            Matching = new Dictionary<int, int>();
            Groups = new Dictionary<int, int>();
            GroupCount = 0;

        }

        public ListGraph(int vertices, bool directed)
        {
            Vertices = vertices;
            IsDirected = directed;

            _graph = new List<SortedDictionary<int, double>>();
            for (int i = 0; i < Vertices; i++)
                _graph.Add(new SortedDictionary<int, double>());
        }

        public ListGraph(int vertices, Dictionary<int, int> edges)
        {
            this.IsWeighted = false;
            this.IsDirected = false;

            Vertices = vertices;

            _graph = new List<SortedDictionary<int, double>>();
            for (int i = 0; i < Vertices; i++)
                _graph.Add(new SortedDictionary<int, double>());

            foreach (KeyValuePair<int, int> kv in edges)
                AddEdge(kv.Key, kv.Value);

            Matching = new Dictionary<int, int>();
            Groups = new Dictionary<int, int>();
            GroupCount = 0;
        }

        public override void AddEdge(int vertex1, int vertex2, double weight = 1.0)
        {
            if (!IsWeighted && weight != 1.0)
                throw new ArgumentException("Weight is not available in non-weighted graphs.");

            if (!_graph[vertex1].ContainsKey(vertex2))
                _graph[vertex1].Add(vertex2, weight);
            if (!IsDirected && !_graph[vertex2].ContainsKey(vertex1))
                _graph[vertex2].Add(vertex1, weight);

            Edges++;
        }

        public override void RemoveEdge(int vertex1, int vertex2)
        {
            _graph[vertex1].Remove(vertex2);
            if (!IsDirected)
                _graph[vertex2].Remove(vertex1);

            Edges--;
        }

        public override List<KeyValuePair<int, int>> GetAllEdges()
        {
            List<KeyValuePair<int, int>> edges = new List<KeyValuePair<int, int>>();

            for (int i = 0; i < _graph.Count; i++)
                foreach (KeyValuePair<int, double> kv in _graph[i])
                    edges.Add(new KeyValuePair<int, int>(i, kv.Key));

            return edges;
        }

        public override bool HasNeighbors(int vertex)
        {
            if (_graph[vertex].Count != 0)
                return true;

            return false;
        }

        public override void Print()
        {
            base.Print();

            for (int i = 0; i < Vertices; i++)
            {
                Console.Write("{0}: ", i);
                foreach (KeyValuePair<int, double> kv in _graph[i])
                    Console.Write("{0} ({1}) ", kv.Key, kv.Value);
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public override object Clone()
        {
            ListGraph copy = new ListGraph(Vertices);

            copy.SetProperties(new[] { IsDirected, IsWeighted, IsConnected, IsComplete, IsBipartite });

            for (int i = 0; i < _graph.Count; i++)
                foreach (KeyValuePair<int, double> kv in _graph[i])
                    copy.AddEdge(i, kv.Key, kv.Value);

            foreach (KeyValuePair<int, int> kv in Matching)
                copy.Matching.Add(kv.Key, kv.Value);

            foreach (KeyValuePair<int, int> kv in Groups)
                copy.Groups.Add(kv.Key, kv.Value);

            copy.GroupCount = GroupCount;

            return copy;
        }

        public override int CalculateDegree(int vertex)
        {
            if (IsDirected)
                throw new InvalidOperationException("Can't calculate degree in directed graph.");

            return _graph[vertex].Count;
        }

        public override int CalculateIncomingEdges(int vertex)
        {
            int edges = 0;

            for (int i = 0; i < Vertices; i++)
                if (_graph[i].ContainsKey(vertex))
                    edges++;

            return edges;
        }

        public override int CalculateOutgoingEdges(int vertex)
        {
            return _graph[vertex].Count;
        }

        public override void SetVertexCount(int vertices)
        {
            Vertices = vertices;

            _graph = new List<SortedDictionary<int, double>>();
            for (int i = 0; i < Vertices; i++)
                _graph.Add(new SortedDictionary<int, double>());

            Matching = new Dictionary<int, int>();
            Groups = new Dictionary<int, int>();
            GroupCount = 0;
        }

        public override bool HasEdge(int vertex1, int vertex2)
        {
            return _graph[vertex1].ContainsKey(vertex2);
        }

        public override double EdgeCost(int vertex1, int vertex2)
        {
            if(!_graph[vertex1].ContainsKey(vertex2))
                throw new InvalidOperationException();

            return _graph[vertex1][vertex2];
        }

        public override List<int> GetNeighbors(int vertex)
        {
            List<int> output = new List<int>();
            foreach (KeyValuePair<int, double> kv in _graph[vertex])
                output.Add(kv.Key);
            return output;
        }
    }
}
