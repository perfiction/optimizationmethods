﻿using System.Collections.Generic;
using Core;

namespace GraphColoring
{
    public static class GraphColoring
    {

        public static int Run(Graph g, out byte[] colors)
        {
            List<int> u = new List<int>(g.Vertices);
            for (int i = 0; i < g.Vertices; i++)
                u.Add(i);

            byte k = 0;
            colors = new byte[g.Vertices];

            while (u.Count != 0)
            {
                k++;

                List<int> w = MaximumIndependentSubset(g, u);

                foreach (int vertex in w)
                {
                    colors[vertex] = k;
                    u.Remove(vertex);
                }
            }

            return k;
        }

        public static List<int> MaximumIndependentSubset(Graph g, List<int> u)
        {
            List<int> u1 = new List<int>(u);
            List<int> w = new List<int>();

            while (u1.Count != 0)
            {
                Graph subGraph = CreateSubGraph(g, u1);

                //find vertex with minimum degree in sub graph
                int vertexWithMinDegree = -1, minDegree = int.MaxValue;
                foreach (int vertex in u1)
                {
                    int degree = subGraph.CalculateDegree(vertex);
                    if (degree < minDegree)
                    {
                        vertexWithMinDegree = vertex;
                        minDegree = degree;
                    }
                }

                //add this vertex to W
                w.Add(vertexWithMinDegree);

                // remove this vertex and all of his neighbors from U1
                u1.Remove(vertexWithMinDegree);
                foreach (int neighbor in g.GetNeighbors(vertexWithMinDegree))
                    u1.Remove(neighbor);
            }

            return w;
        }

        private static Graph CreateSubGraph(Graph g, List<int> vertices)
        {
            Graph subGraph;
            if (g.GetType() == typeof(ArrayGraph))
                subGraph = new ArrayGraph(g.Vertices);
            else
                subGraph = new ListGraph(g.Vertices);
            subGraph.SetProperties(new[] { false, false, true, false, false });

            foreach (KeyValuePair<int, int> edge in g.GetAllEdges())
                if (vertices.Contains(edge.Key) && vertices.Contains(edge.Value))
                    subGraph.AddEdge(edge.Key, edge.Value);

            return subGraph;
        }
    }
}
