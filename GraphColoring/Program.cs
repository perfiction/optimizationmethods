﻿using System;
using System.Collections.Generic;
using Core;

namespace GraphColoring
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"D:\studia\S2\1-sem\MetodyOptymalizacji\INPUT\kolorowanie\prez3.txt";
            Type graphType = typeof(ArrayGraph);

            List<Graph> graphs = FileReader.FromTextReader.ReadGraphsFromFile(filePath, graphType);
            graphs[0].Print();

            int k = GraphColoring.Run(graphs[0], out byte[] colors);

            Console.WriteLine("k = {0}\n\nV - C:", k);
            for(int i = 0; i < colors.Length;i++)
                Console.WriteLine("{0} - {1}", i, colors[i]);

            Console.ReadLine();
        }
    }
}
