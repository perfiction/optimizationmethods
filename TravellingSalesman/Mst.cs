﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;

namespace TravellingSalesman
{
public static class Mst
        {
            public static double Run(Graph g, int startingVertex, out List<KeyValuePair<int, int>> path)
            {
                path = new List<KeyValuePair<int, int>>();
                double pathCost = 0;

                // Find MST using Kruskal algorithm
                double mstCost = Kruskal(g, out var mst);
                Console.WriteLine("Debug: MST edge weights sum: {0} (path cost limit: {1})\n", mstCost, mstCost * 2);
                //mst.Print();

                // Walk MST using DFS and create path through all vertices
                bool[] visited = new bool[mst.Vertices];
                Stack<int> s = new Stack<int>();
                Dfs(mst, startingVertex, s, visited);

                // Calculate cost of created path and format output
                List<int> tmp = s.Reverse().ToList();
                tmp.Add(startingVertex);

                for (int i = 0; i < tmp.Count - 1; i++)
                {
                    int start = tmp[i];
                    int end = tmp[i + 1];
                    path.Add(new KeyValuePair<int, int>(start, end));
                    pathCost += g.EdgeCost(start, end);
                }

                return pathCost;
            }

            public static double Kruskal(Graph g, out Graph mst)
            {
                double cost = 0;
                List<KeyValuePair<int, int>> edges = g.GetAllEdges().OrderBy(x => g.EdgeCost(x.Key, x.Value)).ToList();
                if (g.GetType() == typeof(ArrayGraph))
                    mst = new ArrayGraph(g.Vertices);
                else
                    mst = new ListGraph(g.Vertices);
                mst.SetProperties(new[] { false, true, true, false, false });

                while (mst.Edges < g.Vertices - 1)
                {
                    KeyValuePair<int, int> lowestCostEdge = edges[0];
                    if (!CreatesCycle(mst, lowestCostEdge))
                    {
                        mst.AddEdge(lowestCostEdge.Key, lowestCostEdge.Value, g.EdgeCost(lowestCostEdge.Key, lowestCostEdge.Value));
                        cost += g.EdgeCost(lowestCostEdge.Key, lowestCostEdge.Value);
                    }

                    edges.RemoveAt(0);
                }

                return cost;
            }

            public static void Dfs(Graph g, int vertex, Stack<int> s, bool[] visited)
            {
                s.Push(vertex);
                visited[vertex] = true;
                foreach (int neighbor in g.GetNeighbors(vertex))
                    if (!visited[neighbor])
                        Dfs(g, neighbor, s, visited);
            }

            private static bool CreatesCycle(Graph g, KeyValuePair<int, int> newEdge)
            {
                return FindPath(g, newEdge.Value, newEdge.Key) != null;
            }

            public static List<KeyValuePair<int, int>> FindPath(Graph graph, int source, int destination)
            {
                bool found = false;
                int v = -1;
                int[] p = new int[graph.Vertices];

                Stack<int> s = new Stack<int>();

                bool[] visited = new bool[graph.Vertices];
                for (int i = 0; i < visited.Length; i++)
                    visited[i] = false;

                p[source] = -1;
                s.Push(source);
                visited[source] = true;

                while (s.Count != 0)
                {
                    v = s.Peek();
                    s.Pop();

                    if (v == destination)
                    {
                        found = true;
                        break;
                    }

                    foreach (int pv in graph.GetNeighbors(v))
                    {
                        if (!visited[pv])
                        {
                            p[pv] = v;
                            s.Push(pv);
                            visited[pv] = true;
                        }
                    }
                }

                if (!found)
                    return null;

                List<KeyValuePair<int, int>> ret = new List<KeyValuePair<int, int>>();
                List<int> path = new List<int>();
                while (v != -1)
                {
                    path.Add(v);
                    v = p[v];
                }

                path.Reverse(0, path.Count);

                for (int i = 0; i < path.Count - 1; i++)
                {
                    ret.Add(new KeyValuePair<int, int>(path[i], path[i + 1]));
                }

                return ret;
            }
        }
}
