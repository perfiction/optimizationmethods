﻿using System;
using System.Collections.Generic;
using Core;

namespace TravellingSalesman
{
    public static class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"D:\studia\S2\1-sem\MetodyOptymalizacji\INPUT\komiwojazer\prezentacja.txt";
            Type graphType = typeof(ArrayGraph);

            List<Graph> graphs = FileReader.FromTextReader.ReadGraphsFromFile(filePath, graphType);
            graphs[0].Print();

            double pathCost = Mst.Run(graphs[0], 4, out var path);

            Console.Write("Path cost: {0}\nPath: ", pathCost);
            foreach (KeyValuePair<int, int> edge in path)
                Console.Write("({0} {1}) ", edge.Key, edge.Value);

            //string filePath = @"D:\studia\S2\1-sem\MetodyOptymalizacji\INPUT\komiwojazer\prezentacja.txt";
            //Type graphType = typeof(ArrayGraph);

            //List<Graph> graphs = FileReader.FromTextReader.ReadGraphsFromFile(filePath, graphType);
            //graphs[0].Print();

            //Node answer = BnB.Run(graphs[0]);
            //Dictionary<int, int> path = BnB.FormatOutput(answer, graphs[0], out double cost);

            //Console.Write("Lower bound: {0} (exact cost: {1})\nPath: ", answer.LowerBound, cost);
            //foreach (KeyValuePair<int, int> edge in path)
            //    Console.Write("({0} {1}) ", edge.Key, edge.Value);

            Console.ReadKey();
        }
    }
}
