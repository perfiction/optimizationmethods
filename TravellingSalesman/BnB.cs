﻿using System.Collections.Generic;
using System.Linq;
using Core;

namespace TravellingSalesman
{
    public class Node
    {
        public Dictionary<string, Dictionary<string, double>> CostMatrix { get; }
        public double LowerBound { get; }
        public Dictionary<int, int> CurrentEdges { get; }

        public Node(double lowerBound, Dictionary<string, Dictionary<string, double>> costMatrix, Dictionary<int, int> edges)
        {
            this.LowerBound = lowerBound;
            this.CostMatrix = costMatrix;
            this.CurrentEdges = edges;
        }
    }

    public class AllowDuplicatesComparer : IComparer<double>
    {
        public int Compare(double x, double y)
        {
            int result = x.CompareTo(y);
            if (result == 0)
                return 1;
            else
                return result;
        }
    }

    class BnB
    {
        public static Node Run(Graph g)
        {
            SortedList<double, Node> solutions = new SortedList<double, Node>(new AllowDuplicatesComparer());

            double startingLowerBound = ReduceMatrix(CreateCostMatrix(g), out var startingCosts);
            solutions.Add(startingLowerBound, new Node(startingLowerBound, startingCosts, new Dictionary<int, int>()));

            do
            {
                Node bestNode = solutions.First().Value;
                solutions.RemoveAt(0);

                foreach (string i in bestNode.CostMatrix.Keys)
                {
                    foreach (string j in bestNode.CostMatrix[i].Keys)
                    {
                        if (bestNode.CostMatrix[i][j] != 0)
                            continue;

                        var childCostMatrix = TrimMatrix(bestNode.CostMatrix, i, j);

                        if (childCostMatrix.Count == 0)
                        {
                            Node answer = new Node(bestNode.LowerBound, null, new Dictionary<int, int>(bestNode.CurrentEdges));
                            answer.CurrentEdges.Add(int.Parse(i), int.Parse(j));
                            return answer;
                        }


                        //if (!CanBeReduced(childCostMatrix))
                        //continue;

                        double childLowerBound = ReduceMatrix(childCostMatrix, out childCostMatrix);
                        childLowerBound += bestNode.LowerBound;
                        Node child = new Node(childLowerBound, childCostMatrix, new Dictionary<int, int>(bestNode.CurrentEdges));

                        if (child.CurrentEdges.ContainsKey(int.Parse(i)))
                            continue;

                        child.CurrentEdges.Add(int.Parse(i), int.Parse(j));

                        //Check if new edge (i,j) creates risk of cycle. If yes, then insert Infinity into matrix
                        if (child.CurrentEdges.Count < g.Vertices - 1)
                            PreventCycles(childCostMatrix, bestNode.CurrentEdges, i, j);

                        solutions.Add(child.LowerBound, child);
                    }
                }
            } while (solutions.Count != 0);

            return null;
        }

        public static Dictionary<string, Dictionary<string, double>> CreateCostMatrix(Graph g)
        {
            Dictionary<string, Dictionary<string, double>> costs = new Dictionary<string, Dictionary<string, double>>();
            for (int i = 0; i < g.Vertices; i++)
                costs[i.ToString()] = new Dictionary<string, double>();

            for (int i = 0; i < g.Vertices; i++)
                for (int j = 0; j < g.Vertices; j++)
                    costs[i.ToString()][j.ToString()] = 0;

            foreach (KeyValuePair<int, int> edge in g.GetAllEdges())
                costs[edge.Key.ToString()][edge.Value.ToString()] = g.EdgeCost(edge.Key, edge.Value);

            for (int i = 0; i < costs.Count; i++)
                for (int j = 0; j < costs[i.ToString()].Count; j++)
                    if (costs[i.ToString()][j.ToString()] == 0)
                        costs[i.ToString()][j.ToString()] = double.PositiveInfinity;

            return costs;
        }

        public static double ReduceMatrix(Dictionary<string, Dictionary<string, double>> costs, out Dictionary<string, Dictionary<string, double>> output)
        {
            double lowerBound = 0;
            double min;

            output = new Dictionary<string, Dictionary<string, double>>();
            foreach (string i in costs.Keys)
                output[i] = new Dictionary<string, double>();

            foreach (string i in costs.Keys)
                foreach (string j in costs[i].Keys)
                    output[i][j] = costs[i][j];

            //Find and substract min in every row
            foreach (string i in output.Keys.ToList())
            {
                min = double.MaxValue;

                foreach (string j in output[i].Keys.ToList())
                    if (output[i][j] < min)
                        min = output[i][j];

                foreach (string j in output[i].Keys.ToList())
                    output[i][j] -= min;

                lowerBound += min;
            }

            //Find and substract min in every column
            foreach (string i in costs.First().Value.Keys.ToList())
            {
                min = double.MaxValue;

                foreach (string j in output.Keys.ToList())
                    if (output[j][i] < min)
                        min = output[j][i];

                foreach (string j in output.Keys.ToList())
                    output[j][i] -= min;

                lowerBound += min;
            }

            return lowerBound;
        }

        public static Dictionary<string, Dictionary<string, double>> TrimMatrix(Dictionary<string, Dictionary<string, double>> matrix, string row, string column)
        {
            Dictionary<string, Dictionary<string, double>> output = new Dictionary<string, Dictionary<string, double>>();

            foreach (string i in matrix.Keys)
                if (i != row)
                    output[i] = new Dictionary<string, double>();

            foreach (string i in matrix.Keys)
            {
                if (i == row)
                    continue;

                foreach (string j in matrix[i].Keys)
                {
                    if (j == column)
                        continue;

                    //insert Infinity to output[column][row] => we created path from A to B, so we can't go from B to A anymore
                    if (j == row && i == column)
                        output[i][j] = double.PositiveInfinity;
                    else
                        output[i][j] = matrix[i][j];
                }
            }

            return output;
        }

        public static void PreventCycles(Dictionary<string, Dictionary<string, double>> matrix, Dictionary<int, int> existingEdges, string newEdgeStart, string newEdgeEnd)
        {
            if (existingEdges.ContainsKey(int.Parse(newEdgeEnd)))
            {
                string discardedEdgeStart = existingEdges[int.Parse(newEdgeEnd)].ToString();
                if (matrix.ContainsKey(discardedEdgeStart) && matrix[discardedEdgeStart].ContainsKey(newEdgeStart))
                {
                    string discardedEdgeEnd = newEdgeStart;
                    matrix[discardedEdgeStart][discardedEdgeEnd] = double.PositiveInfinity;
                }

                if (existingEdges.ContainsValue(int.Parse(newEdgeStart)))
                {
                    KeyValuePair<int, int> kv = existingEdges.First(x => x.Value == int.Parse(newEdgeStart));
                    string discardedEdgeEnd = kv.Key.ToString();
                    if (matrix.ContainsKey(discardedEdgeStart) && matrix[discardedEdgeStart].ContainsKey(discardedEdgeEnd))
                    {
                        matrix[discardedEdgeStart][discardedEdgeEnd] = double.PositiveInfinity;
                    }
                }
            }
        }

        public static Dictionary<int, int> FormatOutput(Node node, Graph g, out double cost)
        {
            cost = 0;
            Dictionary<int, int> sortedPath = new Dictionary<int, int>();

            KeyValuePair<int, int> kv = node.CurrentEdges.First(x => x.Key == 0);
            sortedPath.Add(kv.Key, kv.Value);
            cost += g.EdgeCost(kv.Key, kv.Value);

            for (int i = 0; i < node.CurrentEdges.Count - 1; i++)
            {
                kv = node.CurrentEdges.First(x => x.Key == kv.Value);
                sortedPath.Add(kv.Key, kv.Value);
                cost += g.EdgeCost(kv.Key, kv.Value);
            }

            return sortedPath;
        }
    }
}
