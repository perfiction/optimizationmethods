﻿using System;
using System.Collections.Generic;
using Core;

namespace HungarianAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"D:\Studia\S2\1-sem\MetodyOptymalizacji\INPUT\wegierski\test.txt";
            Type graphType = typeof(ArrayGraph);

            List<Graph> graphs = FileReader.FromTextReader.ReadGraphsFromFile(filePath, graphType);
            graphs[0].Print();

            var output = HungarianAlgorithm.ArrayAlgorithm(graphs[0]);

            PrintAnswer(output);

            Console.ReadLine();
        }

        private static void PrintAnswer(List<Tuple<int, int, double>> list)
        {
            double costSum = 0;
            foreach (Tuple<int, int, double> t in list)
            {
                Console.WriteLine("({0} {1}) - {2}", t.Item1, t.Item2, t.Item3);
                costSum += t.Item3;
            }
            Console.WriteLine("Cost sum: {0}", costSum);
        }
    }
}
