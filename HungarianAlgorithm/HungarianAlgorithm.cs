﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using MaximumMatching;

namespace HungarianAlgorithm
{
    public static class HungarianAlgorithm
    {
        public static List<Tuple<int, int, double>> ArrayAlgorithm(Graph g)
        {
            double[,] costMatrix = CreateCostMatrix(g);

            if (costMatrix.GetLength(0) != costMatrix.GetLength(1))
                throw new ArgumentException("Cost matrix must be square.");

            int size = costMatrix.GetLength(0);

            //Substract min ...
            for (int i = 0; i < size; i++)
            {
                // ... in every row (sub min cost in V1)
                double minRow = double.MaxValue;

                for (int j = 0; j < size; j++)
                    if (costMatrix[i, j] < minRow)
                        minRow = costMatrix[i, j];

                for (int j = 0; j < size; j++)
                    costMatrix[i, j] -= minRow;

                // ... in every column (sub min cost in V2)
                double minColumn = double.MaxValue;

                for (int j = 0; j < size; j++)
                    if (costMatrix[j, i] < minColumn)
                        minColumn = costMatrix[j, i];

                for (int j = 0; j < size; j++)
                    costMatrix[j, i] -= minColumn;
            }

            int iter = 0;
            while (true)
            {
                iter++;
                // Cover all 0-s and calculate # of needed lines
                int lines = CalculateLines(costMatrix, out var crossedRows, out var crossedCols);

                // If lines == matrix size, then return the answer
                if (lines == size)
                    return CreateAnswer(g, costMatrix);

                // Else find minimum in free (not-crossed) values
                double notCrossedMin = double.MaxValue;
                for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                        if (!crossedRows.Contains(i) && !crossedCols.Contains(j))
                            if (costMatrix[i, j] < notCrossedMin && costMatrix[i, j] != 0.0 && !double.IsNaN(costMatrix[i,j])) // TODO: 2 ostatnie warunki chyba niepotrzebne
                                notCrossedMin = costMatrix[i, j];
                
                // Then substract min from not-covered values andd add it to (covered) values at the intersection of lines
                for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                    {
                        bool crossedRow = crossedRows.Contains(i);
                        bool crossedCol = crossedCols.Contains(j);

                        if (crossedRow && crossedCol)
                            costMatrix[i, j] += notCrossedMin;
                        else if (!crossedRow && !crossedCol)
                            costMatrix[i, j] -= notCrossedMin;
                    }

                Console.WriteLine("Iteracja {0}", iter);
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        Console.Write(costMatrix[i,j] + " ");
                    }
                    Console.WriteLine();
                }
            }
        }

        public static double[,] CreateCostMatrix(Graph g)
        {
            List<int> v1 = g.Groups.Where(x => x.Value == 0).Select(x => x.Key).ToList();
            List<int> v2 = g.Groups.Where(x => x.Value == 1).Select(x => x.Key).ToList();

            double[,] output = new double[v1.Count, v2.Count];

            foreach (KeyValuePair<int, int> edge in g.GetAllEdges())
            {
                int i = v1.IndexOf(edge.Key);
                int j = v2.IndexOf(edge.Value);
                if (i != -1 && j != -1)
                    output[i, j] = g.EdgeCost(edge.Key, edge.Value);
            }

            //Insert dummy values in place of 0-s
            for (int i = 0; i < output.GetLength(0); i++)
            {
                for (int j = 0; j < output.GetLength(0); j++)
                {
                    if (output[i, j] == 0.0)
                        output[i, j] = double.NaN;
                }
            }

            return output;
        }

        public static List<Tuple<int, int, double>> CreateAnswer(Graph originalGraph, double[,] costMatrix)
        {
            int size = costMatrix.GetLength(0);

            //Create maximum matching in answer
            ArrayGraph g = new ArrayGraph(costMatrix.GetLength(0) * 2);
            g.SetProperties(new[] { false, true, true, false, true });
            g.GroupCount = 2;
            int offset = g.Vertices / 2;
            for (int i = 0; i < g.Vertices; i++)
            {
                if (i < offset)
                    g.Groups.Add(i, 0);
                else
                    g.Groups.Add(i, 1);
            }

            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    if (costMatrix[i, j] == 0.0 && originalGraph.HasEdge(i, j + offset))
                        g.AddEdge(i, j + offset, originalGraph.EdgeCost(i, j + offset));

            MaxMatching.CreateMaximumMatching(g);
            List<Tuple<int, int, double>> output = new List<Tuple<int, int, double>>();
            foreach (KeyValuePair<int, int> matching in g.Matching)
            {
                double cost = g.EdgeCost(matching.Key, matching.Value);
                output.Add(new Tuple<int, int, double>(matching.Key, matching.Value, cost));
            }

            output.Sort((x, y) => x.Item1.CompareTo(y.Item1));

            return output;
        }

        public static int CalculateLines(double[,] costMatrix, out List<int> crossedRows, out List<int> crossedCols)
        {
            // https://en.wikipedia.org/wiki/Hungarian_algorithm#Matrix_interpretation
            int size = costMatrix.GetLength(0);
            int[] assignments = Enumerable.Repeat(-1, size).ToArray();
            bool[] markedCols = new bool[size], markedRows = new bool[size];
            int linesCount = 0;

            // First, assign as many tasks as possible.
            // (create graph and use Maximum Matching algorithm)
            ArrayGraph g = new ArrayGraph(costMatrix.GetLength(0) * 2);
            g.SetProperties(new[] { false, true, true, false, true });
            g.GroupCount = 2;
            int offset = g.Vertices / 2;
            for (int i = 0; i < g.Vertices; i++)
            {
                if (i < offset)
                    g.Groups.Add(i, 0);
                else
                    g.Groups.Add(i, 1);
            }

            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    if (costMatrix[i, j] == 0.0)
                        g.AddEdge(i, j + offset);

            MaxMatching.CreateMaximumMatching(g);
            foreach (KeyValuePair<int, int> matching in g.Matching)
            {
                assignments[matching.Key] = matching.Value - offset;
            }

            Console.WriteLine("Assignments po znalezieniu maksymalneggo skojarzenia:");
            for(int i = 0; i < assignments.Length; i++)
                Console.Write("{0} ", assignments[i]);
            Console.WriteLine();

            // Now to the drawing part.
            // 4. Repeat for all non-assigned rows
            for (int k = 0; k < size; k++)
            {
                if (assignments[k] == -1)
                {
                    // 1. Mark all rows having no assignments
                    for (int i = 0; i < size; i++)
                        if (assignments[i] == -1)
                            markedRows[i] = true;

                    // 2. Mark all (unmarked) columns having zeros in newly marked row(s)
                    for (int i = 0; i < size; i++)
                        for (int j = 0; j < size; j++)
                            if (costMatrix[i, j] == 0.0 && markedRows[i])
                                markedCols[j] = true;

                    // 3. Mark all rows having assignments in newly marked columns
                    for (int i = 0; i < size; i++)
                        if (markedCols[i] && assignments.Contains(i))
                        {
                            int row = Array.IndexOf(assignments, i);
                            markedRows[row] = true;
                        }
                }
            }

            //Now draw lines through all marked columns and unmarked rows.
            crossedCols = new List<int>();
            crossedRows = new List<int>();
            for (int i = 0; i < size; i++)
            {
                if (markedCols[i])
                {
                    linesCount++;
                    crossedCols.Add(i);
                }
                if (!markedRows[i])
                {
                    linesCount++;
                    crossedRows.Add(i);
                }
            }

            Console.WriteLine("Wykreslone wiersze:");
            foreach(int row in crossedRows)
                Console.Write("{0} ", row);
            Console.WriteLine("\nWykreslone kolumny:");
            foreach(int col in crossedCols)
                Console.Write("{0} ", col);
            Console.WriteLine("\n Liczb linii: {0}", linesCount);

            return linesCount;
        }
    }
}
