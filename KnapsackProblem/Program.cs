﻿using System;
using System.IO;

namespace KnapsackProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile(@"D:\studia\S2\1-sem\MetodyOptymalizacji\INPUT\plecakowy\ksiazka.txt", out int n, out int capacity, out int[] values, out int[] weights);

            int[] output = KnapsackProblem.HorowitzSahni(n, capacity, weights, values);

            int totalWeight = 0, profit = 0;

            Console.WriteLine("Capacity: {0}\n", capacity);
            Console.WriteLine("{0,8} {1,8} {2,8}", "Inserted", "Weight", "Value");
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("{0,8} {1,8} {2,8}", output[i] == 1 ? "yes" : "no", weights[i], values[i]);
                profit += output[i] == 1 ? values[i] : 0;
                totalWeight += output[i] == 1 ? weights[i] : 0;
            }
            Console.WriteLine("\nTotal weight: {0}\nProfit: {1}", totalWeight, profit);

            Console.ReadKey();
        }

        private static void ReadFile(string path, out int n, out int cap, out int[] values, out int[] weights)
        {
            FileStream stream = File.OpenRead(path);
            StreamReader reader = new StreamReader(stream);

            n = int.Parse(reader.ReadLine() ?? throw new InvalidOperationException());

            values = new int[n];
            weights = new int[n];

            string[] input = reader.ReadLine()?.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < n; i++)
                if (input != null)
                    values[i] = int.Parse(input[i]);

            input = reader.ReadLine()?.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < n; i++)
                if (input != null)
                    weights[i] = int.Parse(input[i]);

            cap = int.Parse(reader.ReadLine() ?? throw new InvalidOperationException());

            reader.Close();
            stream.Close();
        }
    }
}
