﻿using System.Linq;

namespace KnapsackProblem
{
    public static class KnapsackProblem
    {
        private static int Bound(int itemsCount, int value, int weight, int k, out int newValue, out int newWeight, out int l, int[] inserted, int[] weights, int[] values, int capacity)
        {
            newValue = value;
            newWeight = weight;
            l = k + 1;
            bool B = true;
            while (l < itemsCount && B)
            {
                B = newWeight + weights[l] <= capacity;
                if (B)
                {
                    newValue += values[l];
                    newWeight += weights[l];
                    inserted[l] = 1;
                    l++;
                }
            }

            return B ? newValue : newValue + (capacity - newWeight) * values[l] / weights[l];
        }

        public static int[] HorowitzSahni(int itemsCount, int capacity, int[] weights, int[] values)
        {
            int profit = -1;
            int k = -1, value = 0, weight = 0;
            int l = -1, newValue = 0, newWeight = 0;

            bool B;

            int[] newItemChoice = new int[itemsCount];
            int[] itemChoice = new int[itemsCount];

            do //do ukończenia przegladu tzn. k = 0 (-1)
            {
                do //do momentu gdy mozna uczynic znaczacy krok do przodu
                {
                    B = Bound(itemsCount, value, weight, k, out newValue, out newWeight, out l, newItemChoice, weights, values, capacity) <= profit;
                    if (B) //krok do tylu do ostatniego przedmiotu w plecaku
                    {
                        //znalezc najwieksze j takie, ze inserted[j] = 1 i j <= k
                        int j = -1;
                        for (int i = 0; i < itemsCount; i++)
                            if (newItemChoice[i] == 1 && i <= k)
                                j = i;

                        k = j;
                        if (k > -1) //usuniecie k-tego przedmiotu z plecaka
                        {
                            newItemChoice[k] = 0;
                            value -= values[k];
                            weight -= weights[k];
                        }

                    }
                } while (B && k != -1);

                if (k > -1 || profit == -1) // krok do przodu
                {
                    value = newValue;
                    weight = newWeight;
                    k = l;

                    if (k >= itemsCount) // znaleziono lepsze rozwiazanie
                    {
                        profit = value;
                        k = itemsCount - 1;
                        itemChoice = newItemChoice.ToArray();
                    }
                    else
                    {
                        newItemChoice[k] = 0;
                    }
                }

            } while (k != -1);

            return itemChoice;
        }
    }
}