﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;

namespace EulerianCycle
{
    public static class EulerianCycle
    {
        public static void CalculateR(Graph graph, int startingVertex, Stack<int> output)
        {
            for(int i = 0; i < graph.Vertices; i++)
            {
                while(graph.HasEdge(startingVertex, i))
                {
                    graph.RemoveEdge(startingVertex, i);
                    CalculateR(graph, i, output);
                }
            }
            output.Push(startingVertex);
        }

        public static Stack<int> CalculateI(Graph graph, int startingVertex, Stack<int>output)
        {
            Stack<int> stack = new Stack<int>();

            stack.Push(startingVertex);
            while (stack.Count != 0)
            {
                int v = stack.Peek();
                List<int> vNeighbors = graph.GetNeighbors(v);
                if(vNeighbors.Count == 0)
                {
                    output.Push(v);
                    stack.Pop();
                } else
                {
                    int w = vNeighbors.Last();
                    graph.RemoveEdge(v, w);
                    stack.Push(w);
                }
            }

            return output;
        }

        public static bool CyclePossible(Graph graph)
        {
            if (graph.IsConnected && graph.IsDirected)
            {
                for (int i = 0; i < graph.Vertices; i++)
                    if (graph.CalculateIncomingEdges(i) != graph.CalculateOutgoingEdges(i))
                        return false;
                return true;
            }
            else if (graph.IsConnected && !graph.IsDirected)
            {
                for (int i = 0; i < graph.Vertices; i++)
                {
                    int deg = graph.CalculateDegree(i);
                    if (deg % 2 != 0)
                        return false;
                }
                return true;
            }

            throw new InvalidOperationException("Unknown graph properties.");
        }
    }
}
