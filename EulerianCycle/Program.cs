﻿using System;
using System.Collections.Generic;
using Core;

namespace EulerianCycle
{
    static class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"D:\studia\S2\1-sem\MetodyOptymalizacji\INPUT\cykl_eulera\zaj2.txt";
            Type graphType = typeof(ArrayGraph);

            Graph graph = FileReader.ReadGraphFile(filePath, graphType);
            graph.Print();

            Stack<int> answer = new Stack<int>();
            if (EulerianCycle.CyclePossible(graph))
            {
                Console.Write("Answer: ");
                EulerianCycle.CalculateR(graph, 0, answer);
                while (answer.Count > 0)
                    Console.Write("{0} ", answer.Pop());
                Console.WriteLine();
            } else
            {
                Console.WriteLine("Eulerian cycle is not possible for this graph.");
            }

            Console.ReadLine();
        }
    }
}
