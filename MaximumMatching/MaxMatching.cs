﻿using Core;
using System.Collections.Generic;
using System.Linq;

namespace MaximumMatching
{
    public static class MaxMatching
    {
        public static void CreateMaximumMatching(Graph graph)
        {
            if (graph.Matching == null || !graph.Matching.Any())
                CreateStartingMatching(graph);

            List<KeyValuePair<int, int>> p;

            do
            {
                p = CreateAugmentingPath(graph);
                if (p != null && p.Any())
                {
                    //M ∪ P
                    var sum = new List<KeyValuePair<int, int>>();
                    foreach (KeyValuePair<int, int> matchingEdge in graph.Matching)
                        sum.Add(new KeyValuePair<int, int>(matchingEdge.Key, matchingEdge.Value));

                    foreach (KeyValuePair<int, int> augPathEdge in p)
                    {
                        int keyIndex1 = sum.FindIndex(x => x.Key == augPathEdge.Key);
                        int keyIndex2 = sum.FindIndex(x => x.Key == augPathEdge.Value);
                        if (!(keyIndex1 != -1 && sum[keyIndex1].Value == augPathEdge.Value) && !(keyIndex2 != -1 && sum[keyIndex2].Value == augPathEdge.Key))
                        {
                            sum.Add(new KeyValuePair<int, int>(augPathEdge.Key, augPathEdge.Value));
                            sum.Add(new KeyValuePair<int, int>(augPathEdge.Value, augPathEdge.Key));
                        }
                    }

                    // M ∩ P
                    var common = new List<KeyValuePair<int, int>>();
                    foreach (KeyValuePair<int, int> matchingEdge in graph.Matching)
                        foreach (KeyValuePair<int, int> augPathEdge in p)
                            if (augPathEdge.Key == matchingEdge.Key && augPathEdge.Value == matchingEdge.Value)
                            {
                                common.Add(new KeyValuePair<int, int>(augPathEdge.Key, augPathEdge.Value));
                                common.Add(new KeyValuePair<int, int>(augPathEdge.Value, augPathEdge.Key));
                            }

                    //M' = M ∪ P - M ∩ P (M' = M ⊕ P)
                    foreach (KeyValuePair<int, int> commonEdge in common)
                    {
                        int keyIndex1 = sum.FindIndex(x => x.Key == commonEdge.Key && x.Value == commonEdge.Value);
                        int keyIndex2 = sum.FindIndex(x => x.Key == commonEdge.Value && x.Value == commonEdge.Key);
                        if (keyIndex1 != -1) sum.RemoveAt(keyIndex1);
                        if (keyIndex2 != -1) sum.RemoveAt(keyIndex2);
                    }

                    var m = new Dictionary<int, int>();
                    foreach (KeyValuePair<int, int> kv in sum)
                        m.Add(kv.Key, kv.Value);

                    graph.Matching = m;
                }
            } while (p != null);

            //Remove duplicates [ leave (X,Y) but remove (Y,X) ]
            List<int> keysToRemove = new List<int>();
            foreach (KeyValuePair<int, int> kv in graph.Matching)
                if (graph.Matching.ContainsKey(kv.Value) && graph.Matching[kv.Value] == kv.Key && !(keysToRemove.Contains(kv.Key) || keysToRemove.Contains(kv.Value)))
                    keysToRemove.Add(kv.Value);
            foreach (int key in keysToRemove)
                graph.Matching.Remove(key);
        }

        public static List<KeyValuePair<int, int>> CreateAugmentingPath(Graph graph)
        {
            //Create directed graph
            Graph directed;
            if (graph.GetType() == typeof(ArrayGraph)) directed = new ArrayGraph(graph.Vertices, true);
            else directed = new ListGraph(graph.Vertices, true);

            //Matched = directed edges from V2 to V1
            foreach (KeyValuePair<int, int> kv in graph.Matching)
                if (graph.Groups[kv.Key] == 0)
                    directed.AddEdge(kv.Value, kv.Key);

            //Unmatched = directed edges from V1 to V2
            foreach (KeyValuePair<int, int> kv in graph.GetAllEdges())
                if (graph.Groups[kv.Key] == 1)
                    directed.AddEdge(kv.Value, kv.Key);

            //Create lists of all free (unmatched) vertices in V1 and V2
            List<int> v1Free = new List<int>(), v2Free = new List<int>();
            foreach (KeyValuePair<int, int> kv in graph.Groups)
            {
                if (!graph.HasMatching(kv.Key))
                {
                    if (kv.Value == 0) v1Free.Add(kv.Key);
                    else if (kv.Value == 1) v2Free.Add(kv.Key);
                }
            }

            //Find path from any free vertex in V1 to any free vertex in V2
            List<KeyValuePair<int, int>> output = null;
            foreach (int p1 in v1Free)
            {
                if (output != null) break;
                foreach (int p2 in v2Free)
                {
                    output = FindPath(directed, p1, p2);
                    if (output != null) break;
                }
            }

            if (output == null)
                return null;

            //Find and remove cycles in created path
            List<int> verticesOfCycle = output.GroupBy(x => x.Key).Where(group => group.Count() > 1).Select(group => group.Key).ToList();
            if (verticesOfCycle.Count > 0)
            {
                foreach (int v in verticesOfCycle)
                {
                    int firstIndexOfCycle = output.FindIndex(x => x.Key == v);
                    int lastIndexOfCycle = output.FindLastIndex(x => x.Key == v);

                    if (firstIndexOfCycle == -1 || lastIndexOfCycle == -1)
                        continue;

                    output.RemoveRange(firstIndexOfCycle + 1, lastIndexOfCycle - firstIndexOfCycle);
                }
            }

            return output;
        }

        public static void CreateStartingMatching(Graph graph)
        {
            graph.Matching = new Dictionary<int, int>();

            for (int i = 0; i < graph.Vertices; i++)
                for (int j = 0; j < graph.Vertices; j++)
                    if (!graph.HasMatching(i) && !graph.HasMatching(j) && graph.HasEdge(i, j))
                        graph.AddEdgeToMatching(i, j);
        }

        public static List<KeyValuePair<int, int>> FindPath(Graph graph, int source, int destination)
        {
            bool found = false;
            int v = -1;
            int[] p = new int[graph.Vertices];

            Stack<int> s = new Stack<int>();

            bool[] visited = new bool[graph.Vertices];
            for (int i = 0; i < visited.Length; i++)
                visited[i] = false;

            p[source] = -1;
            s.Push(source);
            visited[source] = true;

            while (s.Count != 0)
            {
                v = s.Peek();
                s.Pop();

                if (v == destination)
                {
                    found = true;
                    break;
                }

                foreach (int pv in graph.GetNeighbors(v))
                {
                    if (!visited[pv])
                    {
                        p[pv] = v;
                        s.Push(pv);
                        visited[pv] = true;
                    }
                }
            }

            if (!found)
                return null;

            List<KeyValuePair<int, int>> ret = new List<KeyValuePair<int, int>>();
            List<int> path = new List<int>();
            while (v != -1)
            {
                path.Add(v);
                v = p[v];
            }

            path.Reverse(0, path.Count);

            for (int i = 0; i < path.Count - 1; i++)
            {
                ret.Add(new KeyValuePair<int, int>(path[i], path[i + 1]));
            }

            return ret;
        }
    }
}
