﻿using System;
using System.Collections.Generic;
using Core;

namespace MaximumMatching
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"D:\studia\S2\1-sem\MetodyOptymalizacji\INPUT\maksymalne_skojarzenie\elearning.txt";
            //string filePath = @"D:\studia\S2\1-sem\MetodyOptymalizacji\INPUT\maksymalne_skojarzenie\test.txt";
            Type graphType = typeof(ArrayGraph);

            List<Graph> graphs = FileReader.FromTextReader.ReadGraphsFromFile(filePath, graphType);
            graphs[0].Print();

            MaxMatching.CreateMaximumMatching(graphs[0]);

            Console.WriteLine("Maximum matching: ");
            foreach (KeyValuePair<int, int> kv in graphs[0].Matching)
            {
                Console.Write("({0} {1}) ", kv.Key, kv.Value);
            }

            Console.ReadLine();
        }
    }
}
